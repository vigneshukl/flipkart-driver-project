package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pages.FindLeadsPageSecondWindow;
import wdMethods.ProjectMethods;

public class FindLeadsPageSecondWindow extends ProjectMethods{

	public FindLeadsPageSecondWindow() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="(//input[@type='text'])[1]") WebElement leadidvalue1;
	@FindBy(xpath="(//button[@class='x-btn-text'])[1]") WebElement clickfindleadsbutton1;
	@FindBy(className="linktext") WebElement clickfirstresultinglead1;
	
	public FindLeadsPageSecondWindow EnterLeadIdValue1(String data) {	
		switchToWindow(1);
		type(leadidvalue1, data);
		return this;
	}
	
	public FindLeadsPageSecondWindow ClickFindLeadsButton1() throws InterruptedException {	
		click(clickfindleadsbutton1);
		Thread.sleep(1000);
		return this;
	}
	
	public MergeLeadsPage ClickFirstResultingLead1() {
		click(clickfirstresultinglead1);
		switchToWindow(0);
		return new MergeLeadsPage();
	}
}
