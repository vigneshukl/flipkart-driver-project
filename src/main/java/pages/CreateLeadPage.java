package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import pages.CreateLeadPage;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="createLeadForm_companyName") WebElement entercompanyname;
	@FindBy(id="createLeadForm_firstName") WebElement enterfirstname;
	@FindBy(id="createLeadForm_lastName") WebElement enterlastname;
	@FindBy(name="submitButton") WebElement clickcreateleadbutton;
	
	@And("Enter CompanyName as (.*)")
	public CreateLeadPage EnterCompanyName(String data) {		
		type(entercompanyname, data);
		return this;
	}
	@And("Enter FirstName as (.*)")
	public CreateLeadPage EnterFirstName(String data)
	{
		type(enterfirstname, data);
		return this;
	}
	@And("Enter LastName as (.*)")
	public CreateLeadPage EnterLastName(String data)
	{
		type(enterlastname, data);
		return this;
	}
	@And("Click on Create Lead")
	public ViewLeadsPage ClickCreateLeadButton()
	{
	 click(clickcreateleadbutton);
	 return new ViewLeadsPage();
	}
	
	








}
