package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pages.MergeLeadsPage;
import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods{

	public MergeLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="(//img[@alt='Lookup'])[1]") WebElement clickfromleadicon;
	@FindBy(xpath="(//img[@alt='Lookup'])[2]") WebElement clicktoleadicon;
	@FindBy(className="buttonDangerous") WebElement clickmergebutton;
	
	public FindLeadsPageFirstWindow ClickFromLeadIcon() {		
		click(clickfromleadicon);
		return new FindLeadsPageFirstWindow();
	}
	
	public FindLeadsPageSecondWindow ClickToLeadIcon() {		
		click(clicktoleadicon);
		return new FindLeadsPageSecondWindow();
	}
	
	public FindLeadsPage ClickMergeButton() {
		click(clickmergebutton);
		acceptAlert();
		return new FindLeadsPage();
	}
	
	

	
	








}
