package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using = "Create Lead")
	WebElement clickcreatelead;
	
	@FindBy(how = How.LINK_TEXT, using = "Merge Leads")
	WebElement clickmergeleads;
	@And("Click CreateLead")
	public CreateLeadPage ClickCreateLead() {		
		click(clickcreatelead);
		return new CreateLeadPage();
	}
	
	public MergeLeadsPage ClickMergeLeads() {		
		click(clickmergeleads);
		return new MergeLeadsPage();
	}
	








}
