package flipkartproject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class flipkartbegineer {
    
    @Test
	public void flipkartwebsite() throws InterruptedException {
    	System.out.println("running");
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
		driver.get("https://www.flipkart.com/");
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		WebElement electronics = driver.findElementByXPath("//span[text()='Electronics']");
		Actions ac=new Actions(driver);
		ac.moveToElement(electronics).perform();
		//Thread.sleep(1000);
		WebElement clickmi = driver.findElementByLinkText("Mi");
		WebDriverWait wait=new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(clickmi)).click();
		//Thread.sleep(2000);
		wait.until(ExpectedConditions.titleContains("Mi Mobile Phones"));
		String pagetitle = driver.getTitle();
		System.out.println(pagetitle);
		if(pagetitle.contains("Mi Mobile Phones"))
		{
			System.out.println("This is the Expected Title");
		}
		else
		{
			System.out.println("Not the expected title:");
		}
		WebElement newestfirst = driver.findElementByXPath("//div[text()='Newest First']");
		//Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(newestfirst)).click();
		Thread.sleep(500);
		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElementsByClassName("_3wU53n")));
		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']")));
		List<WebElement> productname = driver.findElementsByClassName("_3wU53n");
		List<WebElement> productprice = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
		for(WebElement webElement : productname) {
			System.out.println(webElement.getText());
		}
		for(WebElement webElement : productprice) {
			System.out.println(webElement.getText());
		}
		driver.findElementByXPath("//div[@class='_3wU53n'][1]").click();
		Set<String> firstwindow = driver.getWindowHandles();
		List<String> lst=new ArrayList<>();
		lst.addAll(firstwindow);
		driver.switchTo().window(lst.get(1));
		String firstproductpagetitle = driver.getTitle();
		System.out.println(firstproductpagetitle);
		if(firstproductpagetitle.contains("Redmi 6 Pro"))
		{
			System.out.println("This is the Expected first product pageTitle");
		}
		else
		{
			System.out.println("Not the expected title:");
		}
		String ratingsandreviews = driver.findElementByXPath("//span[@class='_35KyD6']//following::span[2]").getText();
		System.out.println(ratingsandreviews);
		driver.quit();
	}

}
