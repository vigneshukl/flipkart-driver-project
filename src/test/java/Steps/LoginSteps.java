//package Steps;
//
//import java.util.concurrent.TimeUnit;
//
//import org.openqa.selenium.chrome.ChromeDriver;
//
//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//
//public class LoginSteps {
//
//	public ChromeDriver driver;
//	@Given("Open the web browser")
//	public void openwebBrowser()
//	{
//		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
//		 driver=new ChromeDriver();
//	}
//	@And("Maximize the browser")
//	public void maximizewebbrowser()
//	{
//		driver.manage().window().maximize();
//	}
//	@And("Set the Timeout")
//	public void settimeout()
//	{
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//	}
//	
//	@And("Launch the URL")
//	public void LaunchtheURL()
//	{
//		driver.get("http://leaftaps.com/opentaps/");
//	}
//	
//	@And("Enter the username as (.*)")
//	public void EnterUsername(String data)
//	{
//		driver.findElementById("username").sendKeys(data);
//	}
//	
//	@And("Enter the password as (.*)")
//	public void EnterPassword(String data)	
//	{
//		driver.findElementById("password").sendKeys(data);
//	}
//	@And("Click on the Login Button")
//	public void ClickLogin()
//	{
//		driver.findElementByClassName("decorativeSubmit").click();
//	}
//	
//	@And("Click on CRMSFA")
//	public void ClickCRMSFA()
//	{
//		driver.findElementByLinkText("CRM/SFA").click();
//	}
//	
//	@And("Click on Leads")
//	public void ClickonLeads()
//	{
//		driver.findElementByLinkText("Leads").click();
//	}
//	@And("Click CreateLead")
//	public void ClickonCreateLead()
//	{
//		driver.findElementByLinkText("Create Lead").click();
//	}
//	@And("Enter CompanyName as (.*)")
//	public void EnterCompanyName(String data)
//	{
//		driver.findElementById("createLeadForm_companyName").sendKeys(data);
//	}
//	@And("Enter FirstName as (.*)")
//	public void EnterFirstName(String data)
//	{
//		driver.findElementById("createLeadForm_firstName").sendKeys(data);
//	}
//	
//	@And("Enter LastName as (.*)")
//	public void EnterLastName(String data)
//	{
//		driver.findElementById("createLeadForm_lastName").sendKeys(data);
//	}
//	
//	@When("Click on Create Lead")
//	public void ClickonCreateLeadbutton()
//	{
//		driver.findElementByName("submitButton").click();
//	}
//	
//	@Then("Lead should be created successfully")	
//	public void VerifyLead()
//	{
//		System.out.println("Success");
//	}
//	
//	@Then("close browser")	
//	public void closebrowser()
//	{
//		driver.close();
//	}
//	
//	
//}
